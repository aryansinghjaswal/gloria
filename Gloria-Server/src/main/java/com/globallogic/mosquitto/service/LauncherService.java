package com.globallogic.mosquitto.service;

import java.util.List;
import java.util.Set;

public interface LauncherService {
	public List<String> getUserIds();
	public Set<String> getGroupsByUserId(String userId);
	public String getAppLauncherData(String userId);
}
