package com.globallogic.mosquitto.service;

import com.globallogic.mosquitto.beans.GlobotMessagePayload;

public interface MessageService {
	public void saveMessage(GlobotMessagePayload globotMessagePayload,String groupId);
	public String getMessagesByGroupId(String groupId);
}
