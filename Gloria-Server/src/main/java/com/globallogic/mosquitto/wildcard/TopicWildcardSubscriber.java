package com.globallogic.mosquitto.wildcard;

import javax.annotation.PostConstruct;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.mosquitto.beans.GlobotMessagePayload;
import com.globallogic.mosquitto.beans.Group;
import com.globallogic.mosquitto.beans.User;
import com.globallogic.mosquitto.service.GloBotService;
import com.globallogic.mosquitto.service.GroupService;
import com.globallogic.mosquitto.service.LauncherService;
import com.globallogic.mosquitto.service.MessageService;
import com.globallogic.mosquitto.service.SubscriberService;
import com.globallogic.mosquitto.utility.MqttPublisher;
import com.google.gson.Gson;

@Service
public class TopicWildcardSubscriber implements MqttCallback{
	
	@Autowired
	SubscriberService subscriberService;
	
	@Autowired
	GloBotService gloBotService;
	
	@Autowired LauncherService launcherService;
	
	@Autowired
	MqttPublisher publisher;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	GroupService groupService;
	private static final Gson gson = new Gson();
	private MqttClient client;
	public TopicWildcardSubscriber(){
		
	}
	
	public void connectionLost(Throwable cause) {
		
	}
	public void messageArrived(String topic, MqttMessage message){
		//System.out.println("Entering mosquitto wildcard");
		if(String.valueOf(message).contains("error")){
			return;
		}
		if(String.valueOf(message).isEmpty()){
			try {
				publisher.publishResponse(topic, "{'error':'Empty payload passed'}",client);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		try{
		GlobotMessagePayload globotMessage = null;
		User user = null;
		switch (topic) {
		case "appLaunched":
			user = gson.fromJson(String.valueOf(message), User.class);
			if(user.getUserId()!=null){
				String launcher = launcherService.getAppLauncherData(user.getUserId());
				publisher.publishResponse(topic,launcher,client);
			}
			break;
		case "createGroup":
			if(String.valueOf(message).contains("status")){
				return;
			}
			Group group = gson.fromJson(String.valueOf(message), Group.class);
			String groupResponse = groupService.saveGroup(group);
			publisher.publishResponse(topic, groupResponse,client);
			break;
		case "subscribeTopic":
			if(String.valueOf(message).contains("systemPublished")){
				return;
			}
			globotMessage = gson.fromJson(String.valueOf(message), GlobotMessagePayload.class);
			String messages = messageService.getMessagesByGroupId(globotMessage.getGroupId());
			publisher.publishResponse(topic, messages,client);
			break;
		case "createUser":
			if(String.valueOf(message).contains("status")){
				return;
			}
			User newUser = gson.fromJson(String.valueOf(message), User.class);
			String tUser = gloBotService.createUser(newUser);
			publisher.publishResponse(topic, tUser,client);
		default:
			if(!publisher.getMessage().equalsIgnoreCase(String.valueOf(message))){
			globotMessage = gson.fromJson(String.valueOf(message), GlobotMessagePayload.class); 
			if(1 == globotMessage.getGlobot()){
				messageService.saveMessage(globotMessage, topic);
			publishGlobotResponse(topic,globotMessage);
			}else{
				if(!"globot".equalsIgnoreCase(globotMessage.getUserId())){
					messageService.saveMessage(globotMessage, topic);
				}
			}
			
			}
			break;
		}
		}
		catch(Exception ex){
			ex.printStackTrace();
			try {
				publisher.publishResponse(topic, "{'error':'Exception occured due to invalid data'}",client);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("Exiting wildcard subscriber");
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
	
	}
	

	
	public void publishGlobotResponse(String topic,GlobotMessagePayload globotMessagePayload){
	
		  String globotResponse = gloBotService.getBotResponse(globotMessagePayload);
	      try {
	    	  publisher.publishResponse(topic,globotResponse,client);
	    	  globotMessagePayload = gson.fromJson(String.valueOf(globotResponse), GlobotMessagePayload.class);
	    	  messageService.saveMessage(globotMessagePayload, topic);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@PostConstruct
	public void initiateWildcardTopicSubscriber(){
		try {
			MemoryPersistence memoryPersistence = new MemoryPersistence();
			client = new MqttClient("tcp://localhost:1883", "subscriber",memoryPersistence);
			MqttConnectOptions connectOptions = new MqttConnectOptions();
			//connectOptions.setKeepAliveInterval(120);
			client.connect(connectOptions);
			client.setCallback(this);
			client.subscribe("#");
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
