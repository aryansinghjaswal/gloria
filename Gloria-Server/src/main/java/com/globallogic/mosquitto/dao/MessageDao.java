package com.globallogic.mosquitto.dao;

import java.util.List;

import com.globallogic.mosquitto.beans.Message;

public interface MessageDao {
	public boolean saveMessage(Message message);
	public List<Message> getMessagesByGroupId(String groupId);
}
