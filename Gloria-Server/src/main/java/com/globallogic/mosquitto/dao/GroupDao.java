package com.globallogic.mosquitto.dao;

import java.util.Set;

import com.globallogic.mosquitto.beans.Group;

public interface GroupDao {
	public Group saveGroup(Group group);

	public boolean addMember(Group group);

	public boolean removeMember(Group group);
	
	public Group getGroupByGroupId(String groupId);
	
	public Set<String> getUsersByGroupId(String groupId);
	
	public String getGroupName(String groupId);
}
