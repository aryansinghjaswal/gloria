package com.globallogic.mosquitto.dao;

import java.util.List;
import java.util.Set;

import com.globallogic.mosquitto.beans.User;

public interface UserDao {
	
	public List<User> getAllUsers();
	public Set<String> getUserGroups(String userId);
	public User getUserByUserId(String userId);
	public String getDeviceByUserId(String userId);
	 public User updateUser(User user);
}
