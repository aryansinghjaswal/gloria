package com.globallogic.mosquitto.serviceImpl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.globallogic.mosquitto.beans.Group;
import com.globallogic.mosquitto.beans.User;
import com.globallogic.mosquitto.dao.GroupDao;
import com.globallogic.mosquitto.dao.UserDao;
import com.globallogic.mosquitto.service.GroupService;
import com.google.gson.Gson;

@PropertySource("classpath:PushNotifications.properties")
@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	Environment env;

	@Autowired
	GroupDao groupDao;

	@Autowired
	UserDao userDao;

	/**
 * 
 */
	@Override
	public String saveGroup(Group group) {
		//Set<String> deviceTokens = new HashSet<String>(0);
		group.setGroupId(group.getGroupName() + group.getGroupOwner());
		Set<String> userIds = group.getMembers();
		/*for (String tempUserId : group.getMembers()) {
			deviceTokens.add(userDao.getDeviceByUserId(tempUserId));
		}
		String message = env.getProperty("pns.message").replace(":groupName",
				group.getGroupName());
		message = message.replace(":groupOwner", group.getGroupOwner());
		PushNotifications.getPushNotificationInstance().pushNotification(
				deviceTokens, message, group.getGroupId());*/
		Set<String> groupIds;
		for(String userId:userIds){
			User user = userDao.getUserByUserId(userId);
			groupIds = user.getGroupIds();
			Set<String> groupIdsCopy = new HashSet<String>(0);
			groupIdsCopy.addAll(groupIds);
			groupIdsCopy.add(group.getGroupId());
			user.setGroupIds(groupIdsCopy);
			userDao.updateUser(user);
		}
		group = groupDao.saveGroup(group);
		if (group!=null) {
			return new Gson().toJson(group,Group.class);
		}
		return "{'error':'error occured during group creation'}";
	}

	/**
 * 
 */
/*	@Override
	public boolean updateGroup(
			Group group) {
		boolean success = false;
		String payloadMessage = "";
		GroupCreationResponse groupCreationResponse = null;
		Group tempGroup = groupDao.getGroupBytopicId(groupOperationsResource
				.getTopicId());
		if(tempGroup == null){
			groupCreationResponse = new GroupCreationResponse(null, new Error(
					"GRO4040", env.getProperty("GRO4040")), true);
			return groupCreationResponse;
		}
		Set<String> userIds = tempGroup.getMembers();
		Set<String> members = new HashSet<String>(0);
		members.addAll(userIds);

		if (groupOperationsResource.getAction().equalsIgnoreCase("add")) {
			members.add(groupOperationsResource.getUserID());
			tempGroup.setMembers(members);
			success = groupDao.addMember(tempGroup);
			payloadMessage = env.getProperty("group.member.addition");
		} else if (groupOperationsResource.getAction().equals("remove")) {
			if(members.contains(groupOperationsResource.getUserID()))
			members.remove(groupOperationsResource.getUserID());
			tempGroup.setMembers(members);
			success = groupDao.removeMember(tempGroup);
			payloadMessage = env.getProperty("group.member.removal");
		} else
			success = false;
		if (success)
			groupCreationResponse = new GroupCreationResponse(new Payload(
					payloadMessage), null, false);
		else
			groupCreationResponse = new GroupCreationResponse(null, new Error(
					"GRO1313", env.getProperty("GRO1313")), true);
		return groupCreationResponse;
	}*/
}
