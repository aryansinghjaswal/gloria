package com.globallogic.mosquitto.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.mosquitto.beans.GlobotMessagePayload;
import com.globallogic.mosquitto.beans.User;
import com.globallogic.mosquitto.dao.UserDao;
import com.globallogic.mosquitto.service.GloBotService;
import com.globot.service.Bot;
import com.google.gson.Gson;

@Service
public class GlobotserviceImpl implements GloBotService{

	@Autowired
	UserDao userDao;
	@Override
	public String getBotResponse(GlobotMessagePayload globotMessagePayload) {
		String[] response = new String[2];
		try{
		Bot bot = Bot.getInstance();
		response = bot.send(globotMessagePayload.getState(), globotMessagePayload.getMessage());
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		String time = String.valueOf(System.currentTimeMillis());
		StringBuilder responseString = new StringBuilder("{ 'state' : '"+response[0]+"',"+
														"'groupId':'"+globotMessagePayload.getGroupId()+"',"+
														"'message':'"+response[1]+"',"+	
														"'time':'"+time+"',"+
														"'globot':"+0+","+
														"'userId':'"+"globot"+	
														"'}");
		return String.valueOf(responseString);
	}

	@Override
	public String createUser(User user) {
		// TODO Auto-generated method stub
		User tempUser = userDao.updateUser(user);
		if(tempUser != null)
		return new Gson().toJson(tempUser,User.class);
		return "{'error':'error occured while adding user.'}";
	}

}
