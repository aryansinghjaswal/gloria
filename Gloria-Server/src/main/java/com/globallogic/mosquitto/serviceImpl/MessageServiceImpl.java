package com.globallogic.mosquitto.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.mosquitto.beans.GlobotMessagePayload;
import com.globallogic.mosquitto.beans.GroupInfo;
import com.globallogic.mosquitto.beans.Message;
import com.globallogic.mosquitto.dao.MessageDao;
import com.globallogic.mosquitto.service.MessageService;
import com.google.gson.Gson;
@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	MessageDao messageDao;
	
	@Override
	public void saveMessage(GlobotMessagePayload globotMessagePayload,
			String groupId) {
		long millisecs = Long.parseLong(globotMessagePayload.getTime());
		Message message = new Message(globotMessagePayload.getGroupId(), 
									  globotMessagePayload.getMessage(), 
									  globotMessagePayload.getUserId(),
									  millisecs);
		messageDao.saveMessage(message);
		
	}

	@Override
	public String getMessagesByGroupId(String groupId) {
		List<Message> messages = messageDao.getMessagesByGroupId(groupId);
		GroupInfo groupInfo = new GroupInfo(messages, true);
		String jsonMessages = new Gson().toJson(groupInfo,GroupInfo.class);
		return jsonMessages;
	}

}
