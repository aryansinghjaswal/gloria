package com.globallogic.mosquitto.cassandra.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.Message;

@Repository
public interface MessageRepository extends CassandraRepository<Message>{
	
	@Query("SELECT * FROM Message")
	public List<Message> getAllMessages();
	
	@Query("SELECT * FROM Message WHERE groupId = ?0 ALLOW FILTERING")
	public List<Message> getMessagesByGroupId(String groupId);
	
	@Query("SELECT * FROM Message WHERE id = 0 ORDER BY id DESC LIMIT 1")
	public List<Message> getLastMessage();
}
