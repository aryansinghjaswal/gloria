package com.globallogic.mosquitto.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.Message;
import com.globallogic.mosquitto.cassandra.repository.MessageRepository;
import com.globallogic.mosquitto.dao.MessageDao;

@Repository
public class MessageDaoImpl implements MessageDao{

	@Autowired
	MessageRepository messageRepository;
	
	public boolean saveMessage(Message message) {
		// TODO Auto-generated method stub
		/*List<Message> tempMes = messageRepository.getLastMessage();
		System.out.println(tempMes);*/
		Message tempMessage = messageRepository.save(message);
		return (tempMessage!=null)?true:false;
	}

	public List<Message> getMessagesByGroupId(String groupId) {
		// TODO Auto-generated method stub
		return messageRepository.getMessagesByGroupId(groupId);
	}

}
