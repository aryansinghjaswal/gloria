package com.globallogic.mosquitto.daoImpl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.Group;
import com.globallogic.mosquitto.cassandra.repository.GroupRepository;
import com.globallogic.mosquitto.dao.GroupDao;

@Repository
public class GroupDaoImpl implements GroupDao {

	@Autowired
	GroupRepository groupRepository;

	@Override
	public Group saveGroup(Group group) {
		Group tempGroup = null;

		tempGroup = groupRepository.save(group);

		return tempGroup;

	}

	@Override
	public boolean addMember(Group group) {

		groupRepository.save(group);

		return true;
	}

	@Override
	public boolean removeMember(Group group) {

		groupRepository.save(group);

		return true;
	}

	@Override
	public Group getGroupByGroupId(String groupId) {
		Group group = null;

		group = groupRepository.getGroupByGroupId(groupId);

		return group;
	}

	@Override
	public Set<String> getUsersByGroupId(String groupId) {
		// TODO Auto-generated method stub
		return groupRepository.getUsersByGroupId(groupId);
	}

	@Override
	public String getGroupName(String groupId) {
		// TODO Auto-generated method stub
		return groupRepository.getGroupName(groupId);
	}

}
