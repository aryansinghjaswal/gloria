package com.globallogic.mosquitto.beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

@Table(value="User")
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 536689707232144303L;
	@PrimaryKeyColumn(name="userId",type=PrimaryKeyType.PARTITIONED,ordinal=0)
	private String userId;
	@Column
	private Set<String> groupIds = new HashSet<String>();
	@Column
	private String deviceToken;
	@Transient
	private boolean status = false;
	
	public User(String userId, Set<String> groupIds, String deviceToken) {
		this.userId = userId;
		this.groupIds = groupIds;
		this.deviceToken = deviceToken;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Set<String> getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(Set<String> groupIds) {
		this.groupIds = groupIds;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	
	
		
}
