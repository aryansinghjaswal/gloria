package com.globallogic.mosquitto.beans;

import java.util.HashSet;
import java.util.Set;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

@Table(value="group")
public class Group {

	@PrimaryKeyColumn(name="groupId",type=PrimaryKeyType.PARTITIONED,ordinal=0)
	private String groupId;
	@Column
	private String groupName;
	@Column
	private Set<String> members = new HashSet<String>();
	@Column
	private String groupOwner;
	@Transient
	private boolean status;
	public Group(){
		
	}
	public Group(String groupId, String groupName, Set<String> members,
			String groupOwner) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
		this.members = members;
		this.groupOwner = groupOwner;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Set<String> getMembers() {
		return members;
	}
	public void setMembers(Set<String> members) {
		this.members = members;
	}
	public String getGroupOwner() {
		return groupOwner;
	}
	public void setGroupOwner(String groupOwner) {
		this.groupOwner = groupOwner;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.groupName+"  "+this.members.toString();
	}
	
	
	
}
