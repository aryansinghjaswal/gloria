package com.globallogic.mosquitto.beans;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author aryan.singh
 *
 */ 
public class OnLaunchGroup {
	private String[] userIds;
	private List<Group> groups = new ArrayList<Group>(0);
	public OnLaunchGroup() {

	}
	
	public String[] getUserIds() {
		return userIds;
	}

	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public boolean addGroup(Group group){
		return this.groups.add(group);
	}
}
