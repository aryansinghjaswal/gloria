package com.globallogic.mosquitto.beans;


public class GlobotMessagePayload {

	private String groupId;
	private String message;
	private String userId;
	private String time;
	private int globot;
	private String state;
	public GlobotMessagePayload(String groupId, String message,
			String userId, String time, int globot, String state) {
		this.groupId = groupId;
		this.message = message;
		this.userId = userId;
		this.time = time;
		this.globot = globot;
		this.state = state;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getGlobot() {
		return globot;
	}
	public void setGlobot(int globot) {
		this.globot = globot;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}
