package com.globallogic.mosquitto.beans;

import java.util.ArrayList;
import java.util.List;

public class GroupInfo {
private List<Message> messages = new ArrayList<Message>(0);
private boolean systemPublished = true;
public List<Message> getMessages() {
	return messages;
}
public void setMessages(List<Message> messages) {
	this.messages = messages;
}
public boolean isSystemPublished() {
	return systemPublished;
}
public void setSystemPublished(boolean systemPublished) {
	this.systemPublished = systemPublished;
}
public GroupInfo(List<Message> messages, boolean systemPublished) {
	this.messages = messages;
	this.systemPublished = systemPublished;
}

}
