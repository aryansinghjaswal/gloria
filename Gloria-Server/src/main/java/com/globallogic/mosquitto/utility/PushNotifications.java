package com.globallogic.mosquitto.utility;

import java.io.IOException;
import java.util.Set;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public final class PushNotifications {

	private PushNotifications() {
	}
	private static final String APP_KEY = "";
	private static PushNotifications pushNotifications = new PushNotifications();
	public static PushNotifications getPushNotificationInstance() {
		return pushNotifications;

	}

	public boolean pushNotification(Set<String> deviceTokens, String message,String groupId) {
		
		try {
            // Register the sender
            Sender sender = new Sender(APP_KEY);
            // prepate the message
            Message gcmMessage = new Message.Builder().timeToLive(30)
                .delayWhileIdle(true).addData(groupId, message).build();
            // send the message to the GCM server.
            for(String deviceToken:deviceTokens){
            Result result = sender.send(gcmMessage, deviceToken, 1);
            System.out.println("push status " + result.toString());
            }
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return true;

	}
}
