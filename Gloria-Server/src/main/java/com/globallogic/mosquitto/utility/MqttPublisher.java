package com.globallogic.mosquitto.utility;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.springframework.stereotype.Service;

@Service
public class MqttPublisher{

	private String message = "";
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
/*	MqttClient mqttClient;
	@PostConstruct
	public void makeConnection(){
		try {
			MemoryPersistence memoryPersistence = new MemoryPersistence();
			mqttClient = new MqttClient("tcp://localhost:1883", "publisher",memoryPersistence);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MqttConnectOptions connectOptions = new MqttConnectOptions();
		//connectOptions.setKeepAliveInterval(120);
		try {
			mqttClient.connect(connectOptions);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mqttClient.setCallback(this);
	}*/
	/*@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}*/
	public void publishResponse(String topic,String message,MqttClient mqttClient){
		System.out.println("Entering publish response");
		//makeConnection();
		MqttTopic mqttTopic = mqttClient.getTopic(topic);
		MqttDeliveryToken token = null;
		try {
			
			token = mqttTopic.publish(message.getBytes(), 0, false);
	    	// Wait until the message has been delivered to the broker
			//token.waitForCompletion();
			this.message=message;
				//mqttClient.disconnect();
			 
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Exiting publish response");
	}
}
