package com.example.cheeku.globot;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Cheeku on 4/16/2016.
 */
public class RequestResponseProcessing {
    ArrayList<String> requesttResponsef;

    public ArrayList<String> processHardCodeCommand(ArrayList<String> matchStrings) {
        requesttResponsef = new ArrayList<String>();
        String response = "I beg your pardon.";
        int maxStrings = matchStrings.size();
        boolean resultFound = false;
        //Constants.Globot=1;
        if (maxStrings != 0) {
            if(Constants.IsWeather)
            {
               // Constants.InList = true;
                requesttResponsef.add(matchStrings.get(0));
            }
            else {
                for (int i = 0; i < Constants.VALID_COMMANDS_SIZE && !resultFound; i++) {
                    for (int j = 0; j < maxStrings && !resultFound; j++) {
                        if (StringUtils.getLevenshteinDistance(matchStrings.get(j).toLowerCase(), Constants.VALID_COMMANDS[i]) < (Constants.VALID_COMMANDS[i].length() / matchStrings.get(j).length())) {
                            if (!MainActivity.isTextModeOn) {
                              /*  if(matchStrings.get(j).toLowerCase().toString().equals("hi glow bought"))
                                {
                                    requesttResponsef.add("Hi Globot");
                                }
                                else if(matchStrings.get(j).toLowerCase().toString().equals("glow bought"))
                                {
                                    requesttResponsef.add("Globot");
                                }
                                else {*/
                                    requesttResponsef.add(matchStrings.get(j));
                               // }
                            }
                            response = getResponse(i);
                            // Constants.Globot=0;
                            //resultFound = true;
                        }
                    }
                }
            }
            requesttResponsef.add(response);
        }
        return requesttResponsef;
    }
    private String getResponse(int command){
        Calendar c = Calendar.getInstance();
        Constants.NeedProcessing = true;
        Constants.InList=false;
        //Constants.Globot=0;
        String retString =  "I'm sorry, Not Possible for me";
        SimpleDateFormat dfDate_day;
        switch (command) {
            case 0:
                Constants.launchCommand = true;
                Constants.InList=true;
                break;
            case 1:
                dfDate_day = new SimpleDateFormat("dd/MM/yyyy");
                retString= " Today is " + dfDate_day.format(c.getTime());
                Constants.InList=true;
                break;
            case 2:
                retString = "I am Gloria";
                Constants.InList=true;
                break;

            case 3:
                retString = "Bye";
                Constants.killCommanded = true;
                Constants.InList=true;
                break;

            case 4:
                retString = "ok";
                Constants.quite = true;
                Constants.InList=true;
                break;

            case 5:
                retString = "and i am back";
                Constants.TALK = true;
                Constants.InList=true;
                break;

            case 6:
                retString = "login";
                Constants.isLoginTry = true;
                Constants.InList=true;
                break;
            case 7://lights on
                retString = "lights on";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 1;
                Constants.IOT_SINGNAL = 1;
                Constants.InList=true;
                break;
            case 8://lights off
                retString = "lights off";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 1;
                Constants.IOT_SINGNAL = 0;
                Constants.InList=true;
                break;
            case 9://tv on
                retString = "radio on";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 10;
                Constants.IOT_SINGNAL = 1;
                Constants.InList=true;
                break;
            case 10://tv off
                retString = "radio off";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 10;
                Constants.IOT_SINGNAL = 0;
                Constants.InList=true;
                break;

            case 11://fan on
                retString = "display on";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 2;
                Constants.IOT_SINGNAL = 1;
                Constants.InList=true;
                break;
            case 12://fan off
                retString = "display off";
                Constants.isIOT = true;
                Constants.IOT_NUMBER = 2;
                Constants.IOT_SINGNAL = 0;
                Constants.InList=true;
                break;

            case 13://fan off
                retString = "Yes";
                Constants.InList=true;
                break;
            case 14://weather
                retString = "weather";
                Constants.InList=true;
                Constants.IsWeather=true;
                break;
            case 15://alarm
                retString = "Please set time";
                Constants.InList=true;
                //Constants.IsWeather=true;
                break;
            default:
                Constants.NeedProcessing = false;
                //Constants.Globot=1;
                Constants.InList=true;
                break;
        }

        return retString;
    }
}
