package com.example.cheeku.globot;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by deepti.arya on 4/14/2016.
 */
public class MyService extends Service
{
    protected AudioManager mAudioManager;
    protected SpeechRecognizer mSpeechRecognizer;
    protected Intent mSpeechRecognizerIntent;
    protected final Messenger mServerMessenger = new Messenger(new IncomingHandler(this));

    protected boolean mIsListening;
    protected volatile boolean mIsCountDownOn;

    static final int MSG_RECOGNIZER_START_LISTENING = 1;
    static final int MSG_RECOGNIZER_CANCEL = 2;

    private int mBindFlag;
    private Messenger mServiceMessenger;

    public RequestResponseProcessing rqst;

    //boolean killCommanded = false, quite = false;

    //legel commands process commands
  /*  private static final String[] VALID_COMMANDS = {
            "talk to me",
            "what day is it",
            "who are you",
            "exit",
            "quite"
    };
*/
    // private static final int VALID_COMMANDS_SIZE = VALID_COMMANDS.length;
    // private Boolean launchCommand = false;
    LocalBroadcastManager broadcaster;
    // static final public String COPA_RESULT = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED";

    // static final public String COPA_MESSAGE = "com.controlj.copame.backend.COPAService.COPA_MSG";

    //static final public String COPA_STATE = "com.controlj.copame.backend.COPAService.COPA_STS";


    BroadcastReceiver receiver;

    @Override
    public void onCreate()
    {
        super.onCreate();
        rqst = new RequestResponseProcessing();
        broadcaster = LocalBroadcastManager.getInstance(this);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());

        //mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    protected static class IncomingHandler extends Handler
    {
        private WeakReference<MyService> mtarget;

        IncomingHandler(MyService target)
        {
            mtarget = new WeakReference<MyService>(target);
        }


        @Override
        public void handleMessage(Message msg)
        {
            final MyService target = mtarget.get();

            switch (msg.what)
            {
                case MSG_RECOGNIZER_START_LISTENING:

                    if (Build.VERSION.SDK_INT >= 16);//Build.VERSION_CODES.JELLY_BEAN)
                {
                    // turn off beep sound
                    target.mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                }
                if (!target.mIsListening)
                {
                    //commented top two lines hit n try.
                    target.mSpeechRecognizer.startListening(target.mSpeechRecognizerIntent);
                    target.mIsListening = true;
                    //Log.d(TAG, "message start listening"); //$NON-NLS-1$
                }
                break;

                case MSG_RECOGNIZER_CANCEL:
                    target.mSpeechRecognizer.cancel();
                    target.mIsListening = false;
                    //Log.d(TAG, "message canceled recognizer"); //$NON-NLS-1$
                    break;
            }
        }
    }

    // Count down timer for Jelly Bean work around
    protected CountDownTimer mNoSpeechCountDown = new CountDownTimer(5000, 5000)
    {

        @Override
        public void onTick(long millisUntilFinished)
        {
            // TODO Auto-generated method stub

        }

        @Override
        public void onFinish()
        {
            mIsCountDownOn = false;
            Message message = Message.obtain(null, MSG_RECOGNIZER_CANCEL);
            try
            {
                mServerMessenger.send(message);
                message = Message.obtain(null, MSG_RECOGNIZER_START_LISTENING);
                mServerMessenger.send(message);
            }
            catch (RemoteException e)
            {

            }
        }
    };

    @Override
    public int onStartCommand (Intent intent, int flags, int startId)
    {
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        try
        {
            Message msg = new Message();
            msg.what = MSG_RECOGNIZER_START_LISTENING;
            mServerMessenger.send(msg);
        }
        catch (RemoteException e)
        {

        }
        return  START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();

        if (mIsCountDownOn)
        {
            mNoSpeechCountDown.cancel();
        }
        if (mSpeechRecognizer != null)
        {
            mSpeechRecognizer.destroy();
        }

    }

    protected class SpeechRecognitionListener implements RecognitionListener
    {

        @Override
        public void onBeginningOfSpeech()
        {
            // speech input will be processed, so there is no need for count down anymore
            if (mIsCountDownOn)
            {
                mIsCountDownOn = false;
                mNoSpeechCountDown.cancel();
            }
            //Log.d(TAG, "onBeginingOfSpeech"); //$NON-NLS-1$
        }

        @Override
        public void onBufferReceived(byte[] buffer)
        {
            String sTest = "";
        }

        @Override
        public void onEndOfSpeech()
        {
            Log.d("TESTING: SPEECH SERVICE", "onEndOfSpeech"); //$NON-NLS-1$
        }

        @Override
        public void onError(int error)
        {
            if (mIsCountDownOn)
            {
                mIsCountDownOn = false;
                mNoSpeechCountDown.cancel();
            }
            mIsListening = false;
            Message message = Message.obtain(null, MSG_RECOGNIZER_START_LISTENING);
            try
            {
                mServerMessenger.send(message);
            }
            catch (RemoteException e)
            {

            }
            //Log.d(TAG, "error = " + error); //$NON-NLS-1$
        }

        @Override
        public void onEvent(int eventType, Bundle params)
        {

        }

        @Override
        public void onPartialResults(Bundle partialResults)
        {

        }

        @Override
        public void onReadyForSpeech(Bundle params)
        {
            if (Build.VERSION.SDK_INT >= 16);//Build.VERSION_CODES.JELLY_BEAN)
            {
                mIsCountDownOn = true;
                mNoSpeechCountDown.start();
                mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            }
            Log.d("TESTING: SPEECH SERVICE", "onReadyForSpeech"); //$NON-NLS-1$
        }

        @Override
        public void onResults(Bundle results)
        {
            //Log.d(TAG, "onResults"); //$NON-NLS-1$

            ArrayList<String> matches = null;
            ArrayList<String> requesttResponsef;
            if(results != null) {

                matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matches != null) {
                    final ArrayList<String> matchesStrings = matches;
                    Constants.NeedProcessing=false;
                    requesttResponsef=rqst.processHardCodeCommand(matchesStrings);
                    sendResult(matchesStrings.get(0).toString(),true);
                    for (int i = 0; i < matchesStrings.size(); i++) {
                        if (matchesStrings.get(i).toLowerCase().contains("gloria")) {
                            Constants.Globot = 1;
                            if (matchesStrings.get(i).toLowerCase().contains("quite")) {
                                Constants.Globot = 0;
                            }
                        }

                    }
                    if (Constants.InList) {
                        //Constants.Globot=0;
                        requesttResponsef = rqst.processHardCodeCommand(matchesStrings);

                        if (Constants.launchCommand) {
                            Intent dialogIntent = new Intent(MyService.this, LoginActivity.class);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(dialogIntent);
                            Constants.launchCommand = false;
                            sendResult("Hello", Constants.NeedProcessing);
                            //launch application
                        } else if (Constants.isLoginTry) {
                            Constants.isLoginTry = false;
                            sendResult(requesttResponsef.get(0), false);
                        } else if (Constants.killCommanded) {
                            Constants.killCommanded = false;
                            sendResult("Bye", false);
                        }
                        else if (Constants.IsWeather) {
                            sendResult(requesttResponsef.get(0), false);
                        }
                        else {
                            if (requesttResponsef.size() == 1)
                                sendResult(requesttResponsef.get(0).toString(), false);
                            else if (requesttResponsef.size() > 1)
                                sendResult(requesttResponsef.get(1).toString(), false);
                        }

                    }
                    else if(Constants.IsWeather)
                    {
                       // if (requesttResponsef.size() == 1)
                            sendResult(requesttResponsef.get(0).toString(), false);
                       // else if (requesttResponsef.size() > 1)
                          //  sendResult(requesttResponsef.get(1).toString(), false);

                    }

                }//uncomment if server ppl cant implement...trust issues
            }
        }

        public void sendResult(String message, Boolean isLeft) {
            mSpeechRecognizer.stopListening();
            Intent intent = new Intent(Constants.COPA_RESULT);
            if(message != null)
                intent.putExtra(Constants.COPA_MESSAGE, message);
            if(Constants.isIOT)
                intent.putExtra(Constants.COPA_IOT,Constants.IOT_NUMBER);
            intent.putExtra(Constants.COPA_STATE, isLeft);
            broadcaster.sendBroadcast(intent);
        }

        @Override
        public void onRmsChanged(float rmsdB)
        {

        }



    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

}