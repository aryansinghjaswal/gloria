package com.example.cheeku.globot.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cheeku.globot.Constants;
import com.example.cheeku.globot.Mqtt.objects.SendMessage;
import com.example.cheeku.globot.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by deepti.arya on 4/27/2016.
 */
public class MessageAdapter extends ArrayAdapter<SendMessage>{

    private TextView chatText, user, time;
    private List<SendMessage> msgs = new ArrayList<SendMessage>();
    private LinearLayout wrapper, txt;


    public void add(SendMessage object) {
        msgs.add(object);

        // super.add(object);
    }

    public MessageAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public int getCount() {
        return this.msgs.size();
    }
    Calendar c = Calendar.getInstance();
    public SendMessage getItem(int index) {
        return this.msgs.get(index);
    }
    private boolean isLeft;
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.display_message, parent, false);
        }

        wrapper = (LinearLayout) row.findViewById(R.id.wrapper1);
        txt = (LinearLayout) row.findViewById(R.id.txt);

        SendMessage msg = msgs.get(position);
        chatText = (TextView) row.findViewById(R.id.comment);
        user = (TextView) row.findViewById(R.id.user);
        time = (TextView) row.findViewById(R.id.dt);
        c.setTimeInMillis(msg.timeMilli);
        user.setText(msg.userId);
        time.setText("  " + c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) +"/"+ c.get(Calendar.YEAR)+
                "  " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));
        chatText.setText(msg.msg);

        if(user.getText().toString().equals(Constants.USERNAME))
            isLeft=true;
        else
            isLeft=false;

        txt.setGravity(isLeft ? Gravity.LEFT : Gravity.RIGHT);
        wrapper.setGravity(isLeft? Gravity.LEFT : Gravity.RIGHT);


        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}

