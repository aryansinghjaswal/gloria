

/**
 * Created by deepti.arya on 5/6/2016.
 */

package com.example.cheeku.globot;

import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cheeku.globot.Mqtt.MqttJava;
import com.example.cheeku.globot.Mqtt.objects.CreateGroup;
import com.example.cheeku.globot.Mqtt.objects.CreatePayload;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

public class CreateGroupDialog extends DialogFragment {
    ListView lv;
    FloatingActionButton dismiss;
    ArrayAdapter<String> arrayAdapter;
    Button btn;
    MqttJava mqttJava;
    EditText grpid, grpname;
    Constants cons = new Constants();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.create_grp_bckgrnd, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mqttJava = new MqttJava();
        // getDialog().setTitle("Participants");
        grpid = (EditText)rootView.findViewById(R.id.grpid);
        grpname = (EditText)rootView.findViewById(R.id.grpname);
        btn = (Button)rootView.findViewById(R.id.part);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whichPart();
            }
        });
        arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1);
        // arrayAdapter.addAll(JsonParser.getSelectedGrp().groupMembers);
        lv = (ListView)rootView.findViewById(R.id.lv);
        lv.setAdapter(arrayAdapter);
        dismiss = (FloatingActionButton) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(seletedItems!=null && seletedItems.size()>0 && !grpname.getText().toString().equals("") && !grpid.getText().toString().equals(""))
                {
                    CreateGroup createGrp = new CreateGroup();
                    createGrp.groupId = grpid.getText().toString()+Constants.USERNAME;
                    createGrp.groupName = grpname.getText().toString();
                    createGrp.GroupOwner = Constants.USERNAME;
                    createGrp.members = participants;
                    String request = CreatePayload.createGroup(createGrp);
                    mqttJava.subscribe("createGroup");
                    mqttJava.sendStringMessage("createGroup", request);
                 /*   SharedPreferences.Editor editor = getActivity().getSharedPreferences("Group", getActivity().MODE_PRIVATE).edit();
                    editor.putString("Group", createGrp.groupId);
                    editor.commit();*/
                    cons.setGroupId(createGrp.groupId, getActivity());
                    // Constants.doRestart(getActivity());
                }
                else
                {
                    Toast.makeText(getActivity(),"*All fields are necessary", Toast.LENGTH_SHORT).show();
                }
                dismiss();
            }
        });
        return rootView;
    }
    CharSequence[] items;
    ArrayList seletedItems;
    ArrayList<String> participants;
    public void whichPart()
    {
        seletedItems = new ArrayList<String>();
        participants = new ArrayList<String>();
        final ArrayList <String> all = JsonParser.getLaunchPayloadResponse().userID;
        items = all.toArray(new CharSequence[all.size()]);
        // items = all.toString();
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle("Participants: ")
                .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            seletedItems.add(indexSelected);
                        } else if (seletedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            seletedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        arrayAdapter.clear();
                        if(seletedItems.size()>0) {
                            for(int i=0; i <seletedItems.size();i++){
                                arrayAdapter.add(all.get(i));
                                participants.add(all.get(i));
                            }
                            lv.setAdapter(arrayAdapter);
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                    }
                }).create();
        dialog.show();
    }
}

