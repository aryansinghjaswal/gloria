package com.example.cheeku.globot.Mqtt.objects;

import java.util.ArrayList;

/**
 * Created by deepti.arya on 4/19/2016.
 */
public class GroupSubscribed {
    public String groupName;
    public String groupId;
    public ArrayList<String> groupMembers;
    public String groupOwner;
    public Boolean status;
}
