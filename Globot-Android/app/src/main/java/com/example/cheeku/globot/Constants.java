package com.example.cheeku.globot;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by Cheeku on 4/16/2016.
 */
public class Constants {

    public static String USERNAME;
    public static Boolean isLoggedIn = false;
    public static Boolean killCommanded = false;
    public static Boolean isLoginTry = false;
    public static Boolean isMeetingTry = false;
    public static Boolean isAddingParticipants = false;
    public static Boolean isProcessAll = false;
    public static Boolean isStartMeeting = false;
    public static Boolean isBLEsupported = false;
    public static Boolean isBLEcheckdone = false;
    public static Boolean isSubscribed = true;
    public static Boolean TALK = false;
    public static Boolean NeedProcessing = false;
    public static Boolean quite = false;
    public static Boolean isIOT = false;
    public static int IOT_NUMBER = 0;
    public static int IOT_SINGNAL = 0;
    public static int Globot = 0;
    public static Boolean ALL = false;
    public static Boolean InList = false;
    public static Boolean IsWeather = false;
    public static final String[] VALID_COMMANDS = {
            "hi gloria",
            "what day is it",
            "who are you",
            "exit",
            "mute",
            "talk",
            "login",
            "switch on the lights",
            "switch off the lights",
            "switch on the radio",
            "switch off the radio",
            "start the display",
            "stop display",
            "gloria",
            "how is the weather",
            "set an alarm"
    };
    public static final int CHECK_CODE = 0x1;
    public static final int LONG_DURATION = 100;
    public static final int SHORT_DURATION = 2000;
    public static final int VALID_COMMANDS_SIZE = VALID_COMMANDS.length;
    public static Boolean launchCommand = false;
    static final public String MQTT_MSG = "com.deepti.mqtt.REQUEST_PROCESSED";
    static final public String COPA_RESULT = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED";
    static final public String COPA_MESSAGE = "com.controlj.copame.backend.COPAService.COPA_MSG";
    static final public String COPA_STATE = "com.controlj.copame.backend.COPAService.COPA_STS";
    static final public String COPA_IOT = "deepti.arya.iot";
    public static String MEETING_NAME = "may i know the name of the meeting please";

    static String TAG ="####error in application restart####";
    public static void doRestart(Context c) {
        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    Intent mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent
                                .getActivity(c, mPendingIntentId, mStartActivity,
                                        PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        //kill the application
                        System.exit(0);
                    } else {
                        Log.e(TAG, "Was not able to restart application, mStartActivity null");
                    }
                } else {
                    Log.e(TAG, "Was not able to restart application, PM null");
                }
            } else {
                Log.e(TAG, "Was not able to restart application, Context null");
            }
        } catch (Exception ex) {
            Log.e(TAG, "Was not able to restart application");
        }
    }

    public String getGroupId(Context context)
    {
        String groupId;
        SharedPreferences prefs = context.getSharedPreferences("Group", context.MODE_PRIVATE);
        String restoredText = prefs.getString("Group", null);
        if (restoredText != null) {
            groupId = prefs.getString("Group", "group1user1");//"No name defined" is the default value.
        }
        else
        {
            groupId = "group1user1";
        }
        return groupId;
    }

    public void setGroupId(String grpid, Context context)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences("Group", context.MODE_PRIVATE).edit();
        editor.putString("Group", grpid);
        editor.commit();
    }
}
