package com.example.cheeku.globot;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.github.clans.fab.FloatingActionButton;

public class Dialog extends DialogFragment {
ListView lv;
    FloatingActionButton dismiss;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_backgrnd, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
       // getDialog().setTitle("Participants");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(JsonParser.getSelectedGrp().groupMembers);
        lv = (ListView)rootView.findViewById(R.id.lv);
        lv.setAdapter(arrayAdapter);

        dismiss = (FloatingActionButton) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;
    }
}