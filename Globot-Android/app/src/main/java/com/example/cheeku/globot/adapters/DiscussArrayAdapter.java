package com.example.cheeku.globot.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cheeku.globot.Mqtt.objects.GroupSubscribed;
import com.example.cheeku.globot.Mqtt.objects.LaunchPayloadResponse;
import com.example.cheeku.globot.OneComment;
import com.example.cheeku.globot.R;

import java.util.ArrayList;
import java.util.List;

public class DiscussArrayAdapter extends ArrayAdapter<GroupSubscribed> {

	private TextView chatText;
	private List<String> groupNames = new ArrayList<String>();
	private List<GroupSubscribed> groupsubs = new ArrayList<GroupSubscribed>();
	private LaunchPayloadResponse launchPayloadResponse = new LaunchPayloadResponse();

	/*public void add(LaunchPayloadResponse object) {
		launchPayloadResponse = object;
		//super.add(object);
	}*/


	public DiscussArrayAdapter(Context context, int textViewResourceId, List<GroupSubscribed> object) {
		super(context, textViewResourceId, object);
		groupsubs = object;
	}

	public int getCount() {
		return this.groupsubs.size();
	}

	public GroupSubscribed getItem(int index) {
		return this.groupsubs.get(index);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.groups_list_row, parent, false);
		}

		chatText = (TextView) row.findViewById(R.id.grp_name);
		chatText.setText(groupsubs.get(position).groupName);
		return row;
	}


}