package com.example.cheeku.globot;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.LocalBroadcastManager;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by deepti.arya on 4/14/2016.
 */
public class Speaker implements OnInitListener {

    private TextToSpeech tts;

    private boolean ready = false;

    private boolean allowed = false;

    private Context context;

    public Speaker(Context context){
        this.context = context;
        tts = new TextToSpeech(context, this);

    }

    public boolean isAllowed(){
        return allowed;
    }

    public void allow(boolean allowed){
        this.allowed = allowed;
    }
    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            // Change this to match your
            // locale
            tts.setLanguage(Locale.US);
            ready = true;
        }else{
            ready = false;
        }

        tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {

            }

            @Override
            public void onDone(String utteranceId) {
                if (!MainActivity.isTextModeOn) {
                    context.startService(new Intent(context, MyService.class));
                }
            }

            @Override
            public void onError(String utteranceId) {

            }
        });
    }
    public void speak(String text){

        // Speak only if the TTS is ready
        // and the user has allowed speech

        if(ready && allowed) {
            HashMap<String, String> hash = new HashMap<String,String>();
            hash.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
                    String.valueOf(AudioManager.STREAM_NOTIFICATION));
            hash.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
                    Speaker.class.getPackage().getName());

            tts.speak(text, TextToSpeech.QUEUE_ADD, hash);
        }
        else
        {
            //MyService.isReadyToStartListening = false;
            // broadcaster = LocalBroadcastManager.getInstance(context);
            //sendResult();
        }
    }

    public void pause(int duration){
        tts.playSilence(duration, TextToSpeech.QUEUE_ADD, null);
    }

    public void destroy(){
        tts.shutdown();
    }

}
