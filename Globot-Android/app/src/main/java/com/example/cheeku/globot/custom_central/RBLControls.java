package com.example.cheeku.globot.custom_central;


import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.example.cheeku.globot.Constants;


@SuppressWarnings("ALL")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class RBLControls {
	private final static String TAG = RBLControls.class.getSimpleName();

	private BluetoothGattCharacteristic characteristicTx = null;
	private RBLService mBluetoothLeService;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mDevice = null;
	private String mDeviceAddress;
	private boolean flag = true;
	private boolean connState = false;
	private boolean scanFlag = false;


	private byte[] data = new byte[3];
	//private static final int REQUEST_ENABLE_BT = 1;
	private static final long SCAN_PERIOD = 2000;

	final private static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	Context context;
	public RBLControls(Activity context)
	{
		this.context = context;
		//this.mBluetoothLeService = mBluetoothLeService;
		//this.mBluetoothAdapter = mBluetoothAdapter;
	}

	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
									   IBinder service) {
			mBluetoothLeService = ((RBLService.LocalBinder) service)
					.getService();
			if (!mBluetoothLeService.initialize()) {
				Log.e(TAG, "Unable to initialize Bluetooth");
				//finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
			mBluetoothAdapter = null;
		}
	};

	public void checkBLESupport()
	{
		final BluetoothManager mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if(!Constants.isBLEcheckdone) {
			Constants.isBLEsupported = true;
			if (!context.getPackageManager().hasSystemFeature(
					PackageManager.FEATURE_BLUETOOTH_LE)) {
				Toast.makeText(context, "Ble not supported", Toast.LENGTH_SHORT)
						.show();
				Constants.isBLEsupported = false;
				//finish();
			}

			if (mBluetoothAdapter == null) {
				Toast.makeText(context, "Ble not supported", Toast.LENGTH_SHORT)
						.show();
				Constants.isBLEsupported = false;
				//finish();
				return;
			}
		}
		if(Constants.isBLEsupported) {
			Constants.isBLEcheckdone = true;
			Intent gattServiceIntent = new Intent(context,
					RBLService.class);
			context.bindService(gattServiceIntent, mServiceConnection, context.BIND_AUTO_CREATE);
		}
	}
	/*public static boolean connectState;
	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (RBLService.ACTION_GATT_DISCONNECTED.equals(action)) {
				Toast.makeText(context, "Disconnected",
						Toast.LENGTH_SHORT).show();
				connectState = false;
			} else if (RBLService.ACTION_GATT_SERVICES_DISCOVERED
					.equals(action)) {
				connectState = true;
				Toast.makeText(context, "Connected",
						Toast.LENGTH_SHORT).show();

				getGattService(mBluetoothLeService.getSupportedGattService());
			} else if (RBLService.ACTION_DATA_AVAILABLE.equals(action)) {
				data = intent.getByteArrayExtra(RBLService.EXTRA_DATA);
				//readAnalogInValue(data);
			} else if (RBLService.ACTION_GATT_RSSI.equals(action)) {
				displayData(intent.getStringExtra(RBLService.EXTRA_DATA));
			}
		}
	};*/

	public void getCharactesticConnected()
	{
		getGattService(mBluetoothLeService.getSupportedGattService());
	}
	public void writeByte(int iotHW, int signal) {
		String value = null;
		byte[] data;
		data = new byte[3];
		//data[0] = 'S';
		if(iotHW == 1)
			data[0] = (byte) 0x01;
		else if(iotHW == 2)
			data[0] = (byte) 0x02;
		else
			data[0] = (byte)0xA0;

			data[1] = (byte) signal;
		//value = new String(data);

		if (data != null) {
			if(characteristicTx != null) {
				Log.d("aaaa:", "" + characteristicTx.setValue(data) + data);
				if (characteristicTx.setValue(data)) {
					final Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							mBluetoothLeService.writeCharacteristic(characteristicTx);
						}
					}, 500);
				}
			}
		}

	}



/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

		if (!getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
					.show();
			finish();
		}
		final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
					.show();
			finish();
			return;
		}

		Intent gattServiceIntent = new Intent(SimpleControls.this,
				RBLService.class);
		bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
		scanLeDevice();

	}
*/


	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_ENABLE_BT
				&& resultCode == Activity.RESULT_CANCELED) {
			finish();
			return;
		}

		else {
			Log.d("MainActivity", "Weird");
			super.onActivityResult(requestCode, resultCode, data);
		}


	}
*/

	/*@Override
	protected void onResume() {
		super.onResume();

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
		if (mBluetoothLeService != null && !connectState) {
			mBluetoothLeService.connect(mDeviceAddress);
		}
		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
	}*/

	private void displayData(String data) {
		writeByte(3, 1);
	}

	/*private void readAnalogInValue(byte[] data) {
		for (int i = 0; i < data.length; i += 3) {
			if (data[i] == 0x0A) {
				if (data[i + 1] == 0x01) {
				}
				else {
				}
			} else if (data[i] == 0x0B) {
				int Value;

				Value = ((data[i + 1] << 8) & 0x0000ff00)
						| (data[i + 2] & 0x000000ff);

			}
		}
	}


*/	private int rssi;

	private void startReadRssi() {
		new Thread() {
			public void run() {

				while (flag) {
					mBluetoothLeService.readRssi();
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			;
		}.start();
	}

	public void getGattService(BluetoothGattService gattService) {
		if (gattService == null)
			return;
		startReadRssi();

		characteristicTx = gattService
				.getCharacteristic(RBLService.UUID_BLE_SHIELD_TX);

		BluetoothGattCharacteristic characteristicRx = gattService
				.getCharacteristic(RBLService.UUID_BLE_SHIELD_RX);
		mBluetoothLeService.setCharacteristicNotification(characteristicRx,
				true);
		mBluetoothLeService.readCharacteristic(characteristicRx);
	}

	public static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();

		intentFilter.addAction(RBLService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(RBLService.ACTION_DATA_AVAILABLE);
		intentFilter.addAction(RBLService.ACTION_GATT_RSSI);

		return intentFilter;
	}

	public void scanLeDevice() {
		new Thread() {

			@Override
			public void run() {
				mBluetoothAdapter.startLeScan(mLeScanCallback);
			}
		}.start();
	}

	public void connectToDevice(BluetoothDevice mDevice) {

		if (mDevice != null && mBluetoothLeService!=null) {
			mDeviceAddress = mDevice.getAddress();
			mBluetoothLeService.connect(mDeviceAddress);
			connState = true;
		} else {
			((Activity)context).runOnUiThread(new Runnable() {
				public void run() {
					Toast toast = Toast
							.makeText(
									context,
									"Couldn't search Ble Shiled device!",
									Toast.LENGTH_SHORT);
					toast.setGravity(0, 0, Gravity.CENTER);
					toast.show();
				}
			});
		}


		System.out.println(connState);
		if (!connState) {
			mBluetoothLeService.connect(mDeviceAddress);
		} else {
		}
	}

	/*@Override
	public void onBackPressed() {
		super.onBackPressed();
		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		disconnectService();
	}*/

	//disconnect ble device
	public void disconnectService() {
		flag = false;
		if(mBluetoothAdapter!=null) {
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
			if (mBluetoothAdapter.isEnabled())
				mBluetoothAdapter.disable();
		}
		if(mBluetoothLeService!=null) {
			mBluetoothLeService.disconnect();
			mBluetoothLeService.close();
		}
		if (mServiceConnection != null)
			context.unbindService(mServiceConnection);

	}

	/*@Override
	protected void onPause() {
		super.onPause();
	}
*/
	protected BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
							 final byte[] scanRecord) {
			String name = device.getName();
			mDeviceAddress = name;
			((Activity)context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					byte[] serviceUuidBytes = new byte[16];
					String serviceUuid = "";
					for (int i = 32, j = 0; i >= 17; i--, j++) {
						serviceUuidBytes[j] = scanRecord[i];
					}
					serviceUuid = bytesToHex(serviceUuidBytes);
					if (device != null && device.getName() != null) {
						if (device.getName().toString() != null) {
							if (device.getName().toString().equals("BLE Mini")) {
								mBluetoothAdapter.stopLeScan(mLeScanCallback);
								mDevice = device;
								Log.d("BLE:::", device.getName());
								connectToDevice(mDevice);
							}

						}
					}
				}
			});
		}

	};

	private String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	private static final int REQUEST_ENABLE_BT = 1;
	public void startBLE()
	{
		if (!mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.enable();
			scanLeDevice();
		}
	}

	/*@Override
	protected void onStop() {
		super.onStop();
		flag = false;
		unregisterReceiver(mGattUpdateReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mServiceConnection != null)
			unbindService(mServiceConnection);
	}*/

}
