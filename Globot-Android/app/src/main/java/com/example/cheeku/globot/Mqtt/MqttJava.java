package com.example.cheeku.globot.Mqtt;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.cheeku.globot.Constants;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.example.cheeku.globot.Mqtt.objects.LaunchPayloadResponse;
import com.example.cheeku.globot.R;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.UUID;


public class MqttJava extends Service implements MqttCallback {

    /*LocalBroadcastManager broadcaster;*/
    static ContextWrapper context;

    public static String getTopic() {
        return topic;
    }

    public static void setTopic(String topic) {
        MqttJava.topic = topic;
    }

    static String topic;

    public MqttClient getClient() {
        return client;
    }

    public void setClient(MqttClient client) {
        this.client = client;
    }

    static MqttClient client;
    JsonParser parse;
    LaunchPayloadResponse launchPayloadResponse;
    public void connect(ContextWrapper context) {
        try {
            this.context = context;
            parse = new JsonParser(context);
           // broadcaster = LocalBroadcastManager.getInstance(context);
            MemoryPersistence persistence = new MemoryPersistence();
            String clientId = UUID.randomUUID().toString();
            MqttClient client = new MqttClient("tcp://13.76.138.197:1883",clientId, persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setConnectionTimeout(60*60*60);
            client.connect(options);
            client.setCallback(this);
            setClient(client);

        } catch (MqttException e) {
            e.printStackTrace();
        }
         }

    public void subscribe(String subs)
    {
        try {
            if(client == null)
            {
                connect(context);
            }
            client = getClient();
            client.subscribe(subs, 0);
            Constants.isSubscribed = true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void sendStringMessage(String subs, String msg)
    {
        try {

            MqttMessage message = new MqttMessage();
            message.setPayload(msg.getBytes());
            message.setQos(0);
            client.publish(subs, message);
        }
        catch (MqttException e)
        {
            e.printStackTrace();
        }
    }

    public void backPressed()
    {
       /* try {
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void connectionLost(Throwable cause) {
        String a = ";";
        Toast.makeText(context,"Mqtt Connection Lost", Toast.LENGTH_LONG).show();
        connect(context);
        subscribe(getTopic());
    }
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.print(message.toString());
      /*  if(!Constants.isSubscribed) {
            Constants.isSubscribed = true;*/
            //Parse appLaunched response
            if (topic.toString().equals("appLaunched")) {
                parse.launchResponseJSONParser(message.toString());
               /* sendResult("appLaunched");*/
            }
            //Parse response of create group
            else if(topic.toString().equals("createGroup"))
            {
                parse.creaetGrp(message.toString());
                //sendResult("createGroup");
            }
            //subscribeTopic
            else if(topic.toString().equals("subscribeTopic"))
            {
                parse.getAllCasandraMessages(message.toString());
           //     sendResult("subscribeTopic");
            }
            //Parse sendMessage Response
            else if(topic.toString().equals(MqttJava.getTopic().toString()))
            {
                parse.sendMessageResponse(message.toString());
               // sendResult("send_message");
            }
       // }
       /* else {
            Constants.isSubscribed = false;
        }*/
        //  main.processResponse(message.getPayload().toString(), false);
    }


  public static void globotNotification(String message) {
      final NotificationManager notificationManager = (NotificationManager)
              context.getSystemService(Context.NOTIFICATION_SERVICE);

      final Notification notification = new Notification(R.drawable.icon,
              "New Message", System.currentTimeMillis());

      // Hide the notification after its selected
      notification.flags |= Notification.FLAG_AUTO_CANCEL;

      final Intent intent = new Intent(context, MqttJava.class);
      final PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);
      notification.setLatestEventInfo(context, "New Message", message, activity);
      notification.number += 1;
      notificationManager.notify(0, notification);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
