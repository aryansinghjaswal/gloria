package com.example.cheeku.globot.Mqtt.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deepti.arya on 4/19/2016.
 */
public class CreatePayload {
//create grp
    public static String createGroup(CreateGroup grp)
    {
       //String grpName = groupName+creatorId;
        JSONObject parentObj = new JSONObject();
        JSONArray mem = new JSONArray();
        for(int i=0;i<grp.members.size();i++)
        {
            mem.put(grp.members.get(i));
        }

        try {
            parentObj.put("groupId", grp.groupId);
            parentObj.put("groupName", grp.groupName);
            parentObj.put("members", mem);
            parentObj.put("groupOwner", grp.GroupOwner);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    public static String getParticipants(ArrayList userIds)
    {
        JSONArray useridsArray = new JSONArray();
        for(int i=0;i<userIds.size();i++) {
            useridsArray.put(userIds.get(i));
        }
        return useridsArray.toString();
    }

//Subscribe topic

    public String subscribeTopicPayload(String userId, String grpName)
    {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserName", userId);
            parentObj.put("GroupName", grpName);
            parentObj.put("State", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    //sending message
    /*
    * Json
    * {'groupId':'group1user1','message':'Aryan','userId':'user1','time':'1234','globot':1,'state':'0'}
    * */
    public static String sendMessagePayload(SendMessage msg)
    {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("groupId", msg.groupId);
            parentObj.put("message", msg.msg);
            parentObj.put("userId", msg.userId);
            parentObj.put("time", msg.timeMilli);
            parentObj.put("globot", msg.globot);
            parentObj.put("state", msg.state);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }


}
