package com.example.cheeku.globot.Mqtt.objects;

import java.util.ArrayList;

/**
 * Created by Cheeku on 4/30/2016.
 */
public class CreateGroup {
    public String groupId;
    public String groupName;
    public ArrayList<String> members;
    public String GroupOwner;
}
