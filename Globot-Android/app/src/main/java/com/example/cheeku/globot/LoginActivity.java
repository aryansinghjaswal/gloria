package com.example.cheeku.globot;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.cheeku.globot.Mqtt.MqttJava;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.example.cheeku.globot.Mqtt.objects.LaunchPayloadResponse;


public class LoginActivity extends AppCompatActivity {
    EditText login;
    // Speaker speaker;
    Button btlogin;
    ProgressDialog progress;
    private final int CHECK_CODE = 0x1;
    // BroadcastReceiver receiver;
    MqttJava mqttJava = new MqttJava();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (EditText)findViewById(R.id.login);
        btlogin = (Button)findViewById(R.id.bt_login);
        mqttJava.connect(this);
      /*  mqttJava.connect(getApplicationContext());
        //startService(new Intent(LoginActivity.this, MqttService.class));
        mqttJava.subscribe("appLaunched");
        mqttJava.sendStringMessage("appLaunched", "{'userId': 'user1'}");
        MqttJava.setTopic("group1user1");
        checkTTS();*/
        new LongOperation().execute();


        btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidUsername(login.getText().toString())) {
                    //  voice("Hello" + Constants.USERNAME);
                    Intent i =new Intent(LoginActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else {
                    //  voice("invalid username please try again");
                }
            }
        });

    }

    public void setUser()
    {
        SharedPreferences prefs = getSharedPreferences("USER", MODE_PRIVATE);
        String restoredText = prefs.getString("user", null);
        if (restoredText != null) {
            Constants.USERNAME = prefs.getString("user", "No");//"No name defined" is the default value.
        }

    }

    public String getGroupId()
    {
        String groupId;
        SharedPreferences prefs = getSharedPreferences("Group", MODE_PRIVATE);
        String restoredText = prefs.getString("Group", null);
        if (restoredText != null) {
            groupId = prefs.getString("Group", "group1user1");//"No name defined" is the default value.
        }
        else
        {
            groupId = "group1user1";
        }
        return groupId;
    }
    public Boolean isValidUsername(String username)
    { Boolean isValid = false;
        if(!username.equals(""))
        {
            LaunchPayloadResponse launchPayloadResponse = JsonParser.getLaunchPayloadResponse();
            if(launchPayloadResponse!=null) {
                for (String user : launchPayloadResponse.userID)
                {
                    if(user.equals(username))
                    {
                        SharedPreferences.Editor editor = getSharedPreferences("USER", MODE_PRIVATE).edit();
                        editor.putString("user", user);
                        editor.commit();
                        Constants.USERNAME = user;
                        isValid = true;
                    }
                    else
                    {
                    }
                }

            }
        }
        return  isValid;
    }
   /* public void voice(String message) {
        //stopService(new Intent(LoginActivity.this, MyService.class));
        speaker.pause(Constants.LONG_DURATION);
        speaker.speak(message);
        speaker.pause(Constants.SHORT_DURATION);
    }*/
   /* private void checkTTS(){
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, CHECK_CODE);
    }*/

    /*    @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if(requestCode == CHECK_CODE){
                if(resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                    speaker = new Speaker(this);
                    speaker.allow(true);

                }else {
                    Intent install = new Intent();
                    install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    startActivity(install);
                }
            }
        }*/
    @Override
    protected void onStart() {
        super.onStart();
       /* LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(Constants.COPA_RESULT)
        );*/
    }

    @Override
    protected void onStop() {
        //  if(speaker!=null)
        // speaker.destroy();
        //  LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            //startService(new Intent(LoginActivity.this, MqttService.class));
            ;

            //checkTTS();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            mqttJava.subscribe("appLaunched");
            mqttJava.sendStringMessage("appLaunched", "{'userId': 'user1'}");
            MqttJava.setTopic(getGroupId());
            setUser();
            if(progress!=null && progress.isShowing())
                progress.dismiss();
            if(Constants.USERNAME != null && !Constants.USERNAME.equals("No"))
            {
                Intent i =new Intent(LoginActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }

        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(LoginActivity.this, "Please wait",
                    "Login...", true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}
