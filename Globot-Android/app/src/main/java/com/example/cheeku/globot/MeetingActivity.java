package com.example.cheeku.globot;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.cheeku.globot.Mqtt.MqttJava;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.example.cheeku.globot.adapters.DiscussArrayAdapter;
import com.github.clans.fab.FloatingActionButton;

public class MeetingActivity extends AppCompatActivity {
    private ListView lv;
    private DiscussArrayAdapter adapter;
    MqttJava mqttJava;
    BroadcastReceiver receiver1;
    Constants cons = new Constants();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);
        mqttJava = new MqttJava();
        lv = (ListView) findViewById(R.id.listView1);
        adapter = new DiscussArrayAdapter(getApplicationContext(), R.layout.listitem_discuss, JsonParser.getLaunchPayloadResponse().groupSubscribeds);
        //adapter.add(new OneComment(false, "Hello!" + Constants.USERNAME));
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JsonParser.setSelectedGrp(JsonParser.getLaunchPayloadResponse().groupSubscribeds.get(position));
                cons.setGroupId(JsonParser.getSelectedGrp().groupId, getApplicationContext());
                MqttJava.setTopic(JsonParser.getSelectedGrp().groupId);
                Intent i =new Intent(MeetingActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabanim);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });
        receiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String response = intent.getStringExtra("Type");
                if (response.equals("createGroup"))
                {
                    mqttJava.subscribe("appLaunched");
                    mqttJava.sendStringMessage("appLaunched", "{'userId': 'user1'}");
                    startActivity(new Intent(MeetingActivity.this, LoginActivity.class));
                }

            }

        };
    }

    public void openDialog()
    {
        FragmentManager fm = getFragmentManager();
        CreateGroupDialog dialogFragment = new CreateGroupDialog();
        dialogFragment.show(fm, "Sample Fragment");
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver1),
                new IntentFilter(Constants.MQTT_MSG)
        );

    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver1);
        super.onStop();
    }
}
