package com.example.cheeku.globot.Mqtt.objects;

import java.util.ArrayList;

/**
 * Created by deepti.arya on 5/6/2016.
 * Response :
 {
 "messages":[
 {
 "id":0,
 "groupId":"group1user1",
 "messagePayload":"Hello aryan, how are you today?",
 "userId":"globot",
 "time":1461996240938
 }
 ],
 "systemPublished":true
 }
 */
public class SubscribeGroupResponse {
public ArrayList<MessagesSubscribeTopic> messages;

}
